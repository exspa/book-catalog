from djoser.views import UserViewSet
from rest_framework.decorators import action
from rest_framework_simplejwt import views as jwt_views

from accounts.mixins import PasswordMixin, UsernameMixin
from .docs import decorators


class CustomUserViewSet(PasswordMixin, UsernameMixin, UserViewSet):
    http_method_names = ['get', 'post', 'delete', 'head', 'options']

    @decorators.swagger_auto_schema_for_user_retrieve
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_user_list
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_user_create
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_user_destroy
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_user_me_get
    @decorators.swagger_auto_schema_for_user_me_destroy
    @action(['get', 'put', 'patch', 'delete'], detail=False)
    def me(self, request, *args, **kwargs):
        return super().me(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_user_activation
    @action(["post"], detail=False)
    def activation(self, request, *args, **kwargs):
        return super().activation(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_user_resend_activation
    @action(['post'], detail=False)
    def resend_activation(self, request, *args, **kwargs):
        return super().resend_activation(request, *args, **kwargs)


class CustomTokenObtainPairView(jwt_views.TokenObtainPairView):
    __doc__ = jwt_views.TokenObtainPairView.__doc__

    @decorators.swagger_auto_schema_for_token_obtain_pair_view
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class CustomTokenRefreshView(jwt_views.TokenRefreshView):
    __doc__ = jwt_views.TokenRefreshView.__doc__

    @decorators.swagger_auto_schema_for_token_refresh_view
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class CustomTokenVerifyView(jwt_views.TokenVerifyView):
    __doc__ = jwt_views.TokenVerifyView.__doc__

    @decorators.swagger_auto_schema_for_token_verify_view
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class CustomTokenBlacklistView(jwt_views.TokenBlacklistView):
    """Takes a token and blacklists it."""

    @decorators.swagger_auto_schema_for_token_blacklist_view
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
