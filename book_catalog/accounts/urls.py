from django.urls import path, include
from rest_framework.routers import SimpleRouter

from . import views


router = SimpleRouter()
router.register('users', views.CustomUserViewSet, basename='users')


urlpatterns = [
    path('auth/jwt/create/', views.CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/jwt/refresh/', views.CustomTokenRefreshView.as_view(), name='token_refresh'),
    path('auth/jwt/verify/', views.CustomTokenVerifyView.as_view(), name='token_verify'),
    path('auth/jwt/blacklist/', views.CustomTokenBlacklistView.as_view(), name='token_blacklist'),
    path('', include(router.urls))
]
