from djoser.serializers import UserSerializer
from rest_framework import serializers


class UserResponseSerializer(UserSerializer):
    pass


class UserDeleteRequestSerializer(serializers.Serializer):
    current_password = serializers.CharField()


class TokenResponseBaseSerializer(serializers.Serializer):
    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class TokenObtainPairResponseSerializer(TokenResponseBaseSerializer):
    access = serializers.CharField()
    refresh = serializers.CharField()


class TokenRefreshResponseSerializer(TokenObtainPairResponseSerializer):
    pass


class TokenVerifyResponseSerializer(TokenResponseBaseSerializer):
    pass


class TokenBlacklistResponseSerializer(TokenResponseBaseSerializer):
    pass
