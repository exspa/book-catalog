from drf_yasg.utils import swagger_auto_schema
from rest_framework import status

from . import serializers


def swagger_auto_schema_for_user_retrieve(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.UserResponseSerializer(),
        },
        operation_summary='Get user by id',
    )(view_func)


def swagger_auto_schema_for_user_list(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.UserResponseSerializer(many=True),
        },
        operation_summary='Get list of users',
    )(view_func)


def swagger_auto_schema_for_user_create(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_201_CREATED: serializers.UserResponseSerializer(),
            status.HTTP_400_BAD_REQUEST: 'Missing required fields or invalid data',
        },
        operation_summary='Register new user',
    )(view_func)


def swagger_auto_schema_for_user_destroy(view_func):
    return swagger_auto_schema(
        request_body=serializers.UserDeleteRequestSerializer,
        responses={
            status.HTTP_204_NO_CONTENT: '',
            status.HTTP_400_BAD_REQUEST: 'Missing required fields or invalid data',
        },
        operation_summary='Delete user by id',
    )(view_func)


def swagger_auto_schema_for_user_me_get(view_func):
    return swagger_auto_schema(
        method='get',
        responses={
            status.HTTP_200_OK: serializers.UserResponseSerializer(),
        },
        operation_summary='Get the authenticated user',
    )(view_func)


def swagger_auto_schema_for_user_me_destroy(view_func):
    return swagger_auto_schema(
        method='delete',
        request_body=serializers.UserDeleteRequestSerializer,
        responses={
            status.HTTP_204_NO_CONTENT: '',
            status.HTTP_400_BAD_REQUEST: 'Missing required fields or invalid data',
        },
        operation_summary='Delete the authenticated user',
    )(view_func)


def swagger_auto_schema_for_user_activation(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_204_NO_CONTENT: '',
            status.HTTP_400_BAD_REQUEST: 'Missing required fields or invalid data',
            status.HTTP_403_FORBIDDEN: 'User is already active',
        },
        operation_summary='Activate user account',
    )(view_func)


def swagger_auto_schema_for_user_resend_activation(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_204_NO_CONTENT: '',
            status.HTTP_400_BAD_REQUEST: 'User is already active',
        },
        operation_summary='Resend the activation e-mail',
    )(view_func)


def swagger_auto_schema_for_token_obtain_pair_view(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.TokenObtainPairResponseSerializer(),
            status.HTTP_400_BAD_REQUEST: 'Missing required fields',
            status.HTTP_401_UNAUTHORIZED: 'Invalid authentication data',
        },
        operation_summary='Obtain access and refresh tokens',
    )(view_func)


def swagger_auto_schema_for_token_refresh_view(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.TokenRefreshResponseSerializer(),
            status.HTTP_400_BAD_REQUEST: 'Missing required fields',
            status.HTTP_401_UNAUTHORIZED: 'Invalid authentication data',
        },
        operation_summary='Refresh access and refresh tokens',
    )(view_func)


def swagger_auto_schema_for_token_verify_view(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.TokenVerifyResponseSerializer(),
            status.HTTP_400_BAD_REQUEST: 'Missing required fields',
            status.HTTP_401_UNAUTHORIZED: 'Invalid authentication data',
        },
        operation_summary='Verify access or refresh token',
    )(view_func)


def swagger_auto_schema_for_token_blacklist_view(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.TokenBlacklistResponseSerializer(),
            status.HTTP_400_BAD_REQUEST: 'Missing required fields',
            status.HTTP_401_UNAUTHORIZED: 'Invalid authentication data',
        },
        operation_summary='Blacklist a refresh token',
    )(view_func)
