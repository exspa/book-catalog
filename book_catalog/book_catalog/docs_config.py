from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator
from drf_yasg.views import get_schema_view

from rest_framework.permissions import AllowAny


DESCRIPTION_API = """This backend application provides API for interact users with a
book catalog, providing functionality to browse books, leave reviews,
and manage favorite books."""


class BothHttpAndHttpsSchemaGenerator(OpenAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.schemes = ['http', 'https']
        return schema


schema_view = get_schema_view(
    openapi.Info(
        title='Book Catalog API',
        default_version='v1',
        description=DESCRIPTION_API,
        terms_of_service='https://books.ru/terms/',
        contact=openapi.Contact(email='support@books.ru'),
        license=openapi.License(name='BSD License'),
    ),
    public=True,
    permission_classes=[AllowAny],
    generator_class=BothHttpAndHttpsSchemaGenerator,
)
