from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Review


@receiver(post_save, sender=Review)
def update_book_rating(sender, instance, **kwargs):
    instance.book.update_average_rating()
