from django.core import validators
from django.db import models


class Author(models.Model):
    first_name = models.CharField('first name', max_length=150)
    last_name = models.CharField('last name', max_length=150)
    date_of_birth = models.DateField('date of birth', blank=True, null=True)

    class Meta:
        verbose_name = 'author'
        verbose_name_plural = 'authors'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Genre(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'genre'
        verbose_name_plural = 'genres'

    def __str__(self):
        return self.name


class Book(models.Model):
    """
    Represents a book with title, description, rating and publication date.
    """

    title = models.CharField('title', max_length=150)
    description = models.TextField('description', blank=True)
    average_rating = models.FloatField('average rating', default=0.0)
    publication_date = models.DateField(verbose_name='publication date')

    author = models.ManyToManyField(Author, related_name='books', verbose_name='author')
    genre = models.ManyToManyField(Genre, related_name='books', verbose_name='genre')

    class Meta:
        verbose_name = 'book'
        verbose_name_plural = 'books'

    def __str__(self):
        return self.title

    def update_average_rating(self):
        """Updates the average rating based on associated reviews."""
        average = self.reviews.aggregate(models.Avg('rating'))['rating__avg']
        self.average_rating = round(average, 2) if average else 0.0
        self.save(update_fields=['average_rating'])


class Review(models.Model):
    """
    Represents a review of a book, with text content and a rating from 1 to 5.
    """

    text = models.TextField(validators=[validators.MaxLengthValidator(1000)])
    rating = models.PositiveSmallIntegerField(
        validators=[validators.MinValueValidator(1), validators.MaxValueValidator(5)]
    )
    created_at = models.DateTimeField(auto_now_add=True)

    book = models.ForeignKey(
        Book, on_delete=models.CASCADE, related_name='reviews', verbose_name='book'
    )
    author = models.ForeignKey(
        'accounts.User',
        on_delete=models.CASCADE,
        related_name='reviews',
        verbose_name='author',
    )

    class Meta:
        verbose_name = 'review'
        verbose_name_plural = 'reviews'

    def __str__(self):
        return f'{self.author.email}, {self.book.title}, {self.rating}'


class Favorite(models.Model):
    """
    Represents a favorite marking of a book by a user.
    """

    added_at = models.DateTimeField(auto_now_add=True)

    user = models.ForeignKey(
        'accounts.User',
        on_delete=models.CASCADE,
        related_name='favorites',
        verbose_name='user',
    )
    book = models.ForeignKey(
        Book,
        on_delete=models.CASCADE,
        related_name='favorites',
        verbose_name='book',
    )

    class Meta:
        verbose_name = 'favorite'
        verbose_name_plural = 'favorites'
        unique_together = ('user', 'book')

    def __str__(self):
        return f'{self.user.email}, {self.book.title}'
