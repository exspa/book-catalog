import django_filters
from .models import Book


class BookFilter(django_filters.FilterSet):
    genre = django_filters.CharFilter(field_name='genre__name', lookup_expr='icontains')
    author_first_name = django_filters.CharFilter(
        field_name='author__first_name', lookup_expr='icontains'
    )
    author_last_name = django_filters.CharFilter(
        field_name='author__last_name', lookup_expr='icontains'
    )
    publication_date = django_filters.DateFromToRangeFilter()

    class Meta:
        model = Book
        fields = ['genre', 'author_first_name', 'author_last_name', 'publication_date']
