from drf_yasg.utils import swagger_auto_schema, no_body
from rest_framework import status

from books import serializers


def swagger_auto_schema_for_book_retrieve(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_200_OK: serializers.BookDetailSerializer(),
            status.HTTP_404_NOT_FOUND: 'Book not found',
        },
        operation_summary='Get book by id',
    )(view_func)


def swagger_auto_schema_for_book_review(view_func):
    return swagger_auto_schema(
        responses={
            status.HTTP_201_CREATED: serializers.ReviewSerializer(),
            status.HTTP_400_BAD_REQUEST: 'Missing required fields or invalid data',
            status.HTTP_401_UNAUTHORIZED: 'Invalid authentication data',
            status.HTTP_404_NOT_FOUND: 'Book not found',
        },
        operation_summary='Create review for book by id',
    )(view_func)


def swagger_auto_schema_for_book_favorite(view_func):
    return swagger_auto_schema(
        request_body=no_body,
        responses={
            status.HTTP_204_NO_CONTENT: '',
            status.HTTP_401_UNAUTHORIZED: 'Invalid authentication data',
            status.HTTP_404_NOT_FOUND: 'Book not found',
        },
        operation_summary='Add a book to favorites by id',
    )(view_func)
