from django.contrib import admin

from . import models


@admin.register(models.Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'last_name',
        'first_name',
        'date_of_birth',
    )
    list_display_links = ('id', 'last_name')
    search_fields = ('last_name',)


@admin.register(models.Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )
    list_display_links = ('id', 'name')
    search_fields = ('name',)


@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'publication_date')
    list_display_links = ('id', 'title')
    list_filter = ('genre',)
    search_fields = ('title',)


@admin.register(models.Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'book',
        'author',
    )
    list_display_links = ('id', 'book', 'author')
    search_fields = ('book', 'author')


@admin.register(models.Favorite)
class FavoriteAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'book',
    )
    search_fields = ('user', 'book')
