from django.db.models import Prefetch
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from . import serializers
from .docs import decorators
from .filters import BookFilter
from .models import Book


class BookViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = serializers.BookListSerializer
    queryset = Book.objects.all()
    permission_classes = [AllowAny]
    filter_backends = [DjangoFilterBackend]
    filterset_class = BookFilter

    def get_queryset(self):
        if self.action == 'list':
            return Book.objects.prefetch_related('author', 'genre')
        elif self.action == 'retrieve':
            return Book.objects.prefetch_related('author', 'genre', 'reviews__author')
        return super().get_queryset()

    def get_permissions(self):
        if self.action == 'review':
            self.permission_classes = [IsAuthenticated]
        elif self.action == 'favorite':
            self.permission_classes = [IsAuthenticated]
        return super().get_permissions()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return serializers.BookDetailSerializer
        elif self.action == 'review':
            return serializers.ReviewSerializer
        elif self.action == 'favorite':
            return serializers.FavoriteSerializer
        return self.serializer_class

    @swagger_auto_schema(operation_summary='Get list of books')
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_book_retrieve
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @decorators.swagger_auto_schema_for_book_review
    @action(['post'], detail=True)
    def review(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(book=self.get_object())
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @decorators.swagger_auto_schema_for_book_favorite
    @action(['post'], detail=True)
    def favorite(self, request, *args, **kwargs):
        book = self.get_object()
        context = self.get_serializer_context()
        context['book'] = book

        serializer = self.get_serializer(data=request.data, context=context)
        serializer.is_valid(raise_exception=True)
        serializer.save(book=book)
        return Response(status=status.HTTP_204_NO_CONTENT)
