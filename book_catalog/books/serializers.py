from rest_framework import serializers
from .models import Book, Author, Genre, Review, Favorite


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['id', 'first_name', 'last_name', 'date_of_birth']


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author_email = serializers.EmailField(source='author.email', read_only=True)
    author_first_name = serializers.CharField(
        source='author.first_name', read_only=True
    )

    class Meta:
        model = Review
        fields = ['id', 'text', 'rating', 'author', 'author_email', 'author_first_name']


class FavoriteSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Favorite
        fields = ['user']

    def validate(self, attrs):
        book = self.context['book']
        if attrs['user'].favorites.filter(book=book).exists():
            raise serializers.ValidationError(
                f'User already has book "{book}" in favorites',
            )
        return attrs


class BookListSerializer(serializers.ModelSerializer):
    is_favorite = serializers.SerializerMethodField()

    author = AuthorSerializer(many=True, read_only=True)
    genre = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = ['id', 'title', 'author', 'genre', 'average_rating', 'is_favorite']

    @property
    def current_user(self):
        return self.context.get('request').user

    def get_is_favorite(self, obj):
        """Determines if the book is marked as a favorite by the given user."""
        user = self.current_user
        if not hasattr(self, '_favorites'):
            if user.is_authenticated:
                self._favorites = set(
                    Favorite.objects.filter(user=user).values_list('book_id', flat=True)
                )
            else:
                self._favorites = set()
        return obj.id in self._favorites

    def to_representation(self, instance):
        """Removes 'is_favorite' if user is unauthenticated."""
        representation = super().to_representation(instance)
        if not self.current_user.is_authenticated:
            representation.pop('is_favorite', None)
        return representation


class BookDetailSerializer(BookListSerializer):
    reviews = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = '__all__'
