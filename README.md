# [Book Catalog]()


## Содержание

[1. Описание проекта](README.md#Описание-проекта)
[2. Функционал](README.md#Функционал)
[3. Установка и запуск](README.md#Установка-и-запуск)


## Описание проекта

Пример простого бэкенд приложения, которое реализует API для взаимодействия пользователей с каталогом книг. Функционал: просмотр информации о книгах, можно оставить отзыв, оценить книгу или добавить в список избранных.

:arrow_up: [к содержанию](README.md#Содержание)


## Функционал

#### Реализация и используемые технологии:

- Фреймворк `Django` + `Django Rest Framework`
- Хранение данных - `SQLite`
- Документация API на базе Swagger, ReDoc - `drf-yasg`
- Управление пользователями API, регистрация/авторизация по JWT токену - `djoser`

:arrow_up: [к содержанию](README.md#Содержание)


## Установка и запуск

#### Клонируйте проект локально и перейдите в папку `book_catalog`:

```bash
git clone https://gitlab.com/exspa/book-catalog.git
cd book_catalog
```

#### Создайте виртуальное окружение для проекта:

```bash
python -m venv venv
source venv/bin/activate
```

#### Установите необходимые пакеты из файла `book_catalog/requirements.txt`:
```bash
pip install -r book_catalog/requirements.txt
```

#### Сгенерировать секретный ключ Django:

```bash
python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
```

#### В файле `book_catalog/.env.Example` замените шаблонные переменные окружения django, `SECRET_KEY` обязательно, остальные на усмотрение:

```python
# django
SECRET_KEY='django-example-_137^&*6*98_-38y47hh712-=7&738&&11'
DEBUG=True
ALLOWED_HOSTS='example.com'
```

#### Создайте и примените миграции из каталога проекта `/book_catalog/`:
```bash
python manage.py makemigrations
python manage.py migrate
```

#### Запуск проекта, выполните команды в отдельных консольных окнах:

 - `python manage.py runserver` - запуск Django-проекта из каталога проекта */book_catalog/*

:arrow_up: [к содержанию](README.md#Содержание)
